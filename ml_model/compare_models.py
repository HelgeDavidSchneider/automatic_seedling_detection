"""
This script fits multiple classification models to the trainingsdata and saves the achieved accuracy score
in a dedicated log file.
"""

import os
import numpy as np
from sklearn import metrics, svm
from sklearn.model_selection import KFold, train_test_split, cross_val_score, cross_validate
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from trainings_data.create_trainigs_data import get_array_from_samples


# Define path where the trainingsdata is located
trainings_path = os.path.normpath('E:/Trainings_Data/ABP_Eucalyptus')
log_path = os.path.join(trainings_path, 'log.txt')
with open(log_path, 'w+') as f:
    f.write('')


# Get images as flattened arrays
n_samples = 10000
eucalyptus = get_array_from_samples(os.path.join(trainings_path, 'eucalyptus'), n_max=n_samples)
ground = get_array_from_samples(os.path.join(trainings_path, 'ground'), n_max=n_samples)
weed = get_array_from_samples(os.path.join(trainings_path, 'weed'), n_max=n_samples)

# Create data and target array
data = np.concatenate((eucalyptus, ground, weed), axis=0)  # Concatenate data to single array
targets = np.array([0] * eucalyptus.shape[0] + [1] * ground.shape[0] + [2] * weed.shape[0])

# Fit different classifiers to the data
for classifier in [RandomForestClassifier(n_jobs=-1),
                   LogisticRegression(n_jobs=-1),
                   svm.SVC(n_jobs=-1)]:

    # Get accuracy of the model from cross validation
    cv_results = cross_val_score(classifier, data, targets, cv=KFold(n_splits=10, shuffle=True), scoring='accuracy')

    # Save results in log file
    with open(log_path, 'a') as f:
        f.write(classifier.__class__.__name__)

        for acc in cv_results:
            f.write(f',{str(acc)}')

        f.write('\n')


