import os
import numpy as np
import pandas as pd
from accuracy_assessment.accuracy_assessment import accuracy_assessment


# Import Trainingsdata sheet
sheet_path = 'create_trainings_data/sheets/abp_eucalyptus_with_ref.csv'
ds = pd.read_csv(sheet_path, sep=';')
ds = ds.replace(np.nan, '', regex=True)  # Replace nan with empty string

# Define target directories
ortho_dir = os.path.abspath('E:/Ortho')
results_dir = os.path.abspath('E:/Results')
ref_path = os.path.abspath('E:/Ref_Data')

# Join files with target directories
ds['ortho_path'] = [os.path.join(ortho_dir, file) for file in ds['Ortho_Name']]
ds['det_points'] = [os.path.join(results_dir,
                                 f"{os.path.splitext(file)[0]}/"
                                 f"{os.path.splitext(file)[0]}_filtered_features.shp")
                    for file in ds['Ortho_Name']]
ds['ref_points_path'] = [os.path.join(ref_path, file + '.shp') for file in ds['Ref Points']]
ds['ref_poly_path'] = [os.path.join(ref_path, file + '.shp') for file in ds['Ref Polygons']]

# Create log to store the results
with open('accuracy_assessment/acc_log.txt', 'a') as f:
    f.write(f'Overall Accuracy,' \
            f'Detected Ref Points,Double Classified Ref Points,Missed Ref Points,' \
            f'Correct Points,Double Classified Points,False Points\n')


# Iterate over orthos
for ortho_path, det_points_path, ref_points_path, ref_plots_path \
        in zip(ds['ortho_path'], ds['det_points'], ds['ref_points_path'], ds['ref_poly_path']):

    # Check if reference data exists
    if os.path.isfile(ref_points_path) and os.path.isfile(det_points_path):

        # Execute accuracy assessment
        acc_ass_result = accuracy_assessment(ortho_path, det_points_path, ref_points_path, ref_plots_path)

        # Log the result of the accuracy assessment
        ortho_name = os.path.splitext(os.path.basename(ortho_path))[0]

        with open('accuracy_assessment/acc_log.txt', 'a') as f:
            f.write(f'{ortho_name},{acc_ass_result}\n')
    else:
        print("nope")
