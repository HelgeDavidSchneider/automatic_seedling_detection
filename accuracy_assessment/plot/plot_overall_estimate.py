import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 22})


ds = [0.9545454545454546, 1.239766081871345, 0.7175572519083969, 0.9495412844036697,
      0.8834355828220859, 1.5661157024793388, 0.8944591029023746, 0.8280254777070064,
      0.8599033816425121, 0.7073170731707317]


fig, ax = plt.subplots()
ax.boxplot(ds, widths=0.4, patch_artist=True)
ax.set_xlabel('')
ax.set_xticklabels(['Referenzprojekte'])
ax.set_ylabel(r'$\frac{Anzahl Referenzsetzlinge}{Anzahl Erkannte Setzlinge}$', fontsize=25)
ax.set_ylim(0, max(ds))
ax.grid(True)
