"""
This script iterates over all orthos and shapefiles and performs several operations on them.
"""

import os
import numpy as np
import pandas as pd
from rasterio import open


# Import Trainingsdata sheet
sheet_path = 'create_trainings_data/sheets/abp_eucalyptus.csv'
ds = pd.read_csv(sheet_path, sep=';')
ds = ds.replace(np.nan, '', regex=True)  # Replace nan with empty string

# Append file type extenstions and join paths
ds['Ortho_Name'] = ds['Ortho_Name'] + '.tif'
ds['Train_Name'] = ds['Train_Name'] + '.shp'

# Define target directories
ortho_dir = os.path.abspath('E:/Ortho')
shape_dir = os.path.abspath('E:/Shape')

# Join files with target directories
ds['ortho_path'] = [os.path.join(ortho_dir, file) for file in ds['Ortho_Name']]
ds['train_path'] = [os.path.join(shape_dir, file) for file in ds['Train_Name']]

# Get some statistics about every project
x_width = []
y_width = []
size = []
area = []

for f in ds.ortho_path:
    if os.path.exists(f):

        # Get height, width and size of the raster
        raster = open(f)
        x_width.append(raster.height)
        y_width.append(raster.width)
        size.append(int(os.path.getsize(f) / 1000))

        # Get area in m^2
        transform = raster.transform
        area.append(transform[0] * raster.width * - transform[4] * raster.height)
    else:
        raise ValueError(f'File {f} does not exist.')


# Assign data to data frame
ds['Area'] = area
ds['x_width'] = x_width
ds['y_width'] = y_width
ds['size'] = size

ds.to_csv('create_trainings_data/sheets/abp_eucalyptus_with_stats.csv')
