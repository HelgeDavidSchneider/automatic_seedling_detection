import os
import pandas as pd
import matplotlib.pyplot as plt


# Get the result from the model fitting
trainings_path = os.path.normpath('E:/Trainings_Data/ABP_Eucalyptus')
log_path = os.path.join(trainings_path, 'log.txt')

df = pd.read_csv(log_path, header=None)
df = df.T
df = df.rename(columns=df.iloc[0]).drop(df.index[0])

# Slighty increase font size of plot
plt.rcParams.update({'font.size': 14})
ax = df.plot(kind='box')
ax.set_ylim(0.92, 0.98)
ax.grid(True)
ax.set_title('Vergleich der Modellgenauigkeit')
ax.set_ylabel('Modellgenauigkeit')
ax.set_xticklabels(['Random Forest', 'Logistische Regression', 'SVM'])
