"""
This script fits a classification model to the trainingsdata and saves it so it can be used for further projects.
"""

import os
import numpy as np
from sklearn import metrics, svm
from sklearn.model_selection import train_test_split
import joblib
import matplotlib.pyplot as plt
from trainings_data.create_trainigs_data import get_array_from_samples


# Get images as flattened arrays
trainings_path = os.path.normpath('E:\\Trainings_Data\ABP_Eucalyptus')
n_samples = 10000
eucalyptus = get_array_from_samples(os.path.join(trainings_path, 'eucalyptus'), n_max=n_samples)
ground = get_array_from_samples(os.path.join(trainings_path, 'ground'), n_max=n_samples)
weed = get_array_from_samples(os.path.join(trainings_path, 'weed'), n_max=n_samples)

# Create data and target array
data = np.concatenate((eucalyptus, ground, weed), axis=0)  # Concatenate data to single array
targets = np.array([0] * eucalyptus.shape[0] + [1] * ground.shape[0] + [2] * weed.shape[0])
x_train, x_test, y_train, y_test = train_test_split(data, targets, test_size=0.35, shuffle=True)

# Create a support vector classifier_model and predict data
classifier = svm.SVC(kernel='rbf')
classifier.fit(x_train, y_train)  # Fit model to the trainings data
predicted = classifier.predict(x_test)  # Predict result for test images

# Print Classification Report
print(f"Classification report for classifier_model {classifier}\n"
      f"{metrics.classification_report(y_test, predicted)}")

# Plot Confusion Matrix
fig, ax = plt.subplots()
disp = metrics.plot_confusion_matrix(classifier, x_test, y_test, ax=ax)
ax.set_title("Konfusions Matrix")
ax.set_xlabel('Vorhergesagte Klasse')
ax.set_ylabel('Wahre klasse')
ax.set_xticklabels(['Setzling', 'Boden', 'Unkraut'])
ax.set_yticklabels(['Setzling', 'Boden', 'Unkraut'], rotation=90)
plt.rcParams.update({'font.size': 16})

# Save classifier_model
joblib.dump(classifier, 'classifier_model/svc_eucalyptus_gtfp_3px_radius.joblib.pkl', compress=9)
