"""
This script compares the performace of multiple hyperparameters on the same model and writes it to a log file.
"""

import os
import numpy as np
from sklearn import svm
from sklearn.metrics import f1_score, make_scorer
from sklearn.model_selection import train_test_split
from trainings_data.create_trainigs_data import get_array_from_samples


# Get images as flattened arrays
trainings_path = os.path.normpath('E:/Trainings_Data/ABP_Eucalyptus')
n_samples = 1000
eucalyptus = get_array_from_samples(os.path.join(trainings_path, 'eucalyptus'), n_max=n_samples)
ground = get_array_from_samples(os.path.join(trainings_path, 'ground'), n_max=n_samples)
weed = get_array_from_samples(os.path.join(trainings_path, 'weed'), n_max=n_samples)

# Create data and target array
data = np.concatenate((eucalyptus, ground, weed), axis=0)
targets = np.array([0] * eucalyptus.shape[0] + [1] * ground.shape[0] + [2] * weed.shape[0])
x_train, x_test, y_train, y_test = train_test_split(data, targets, test_size=0.35, shuffle=True)
f1 = make_scorer(f1_score, average='weighted')

# Create log
log_path = 'hyperparameter_tuning/hyperparameter.txt'
with open(log_path, 'w+') as f:
    f.write(f'C,Kernel,Gamma,Score\n')

kernel = 'rbf'


# Evaluate the hyperaparameters
for c_value in [1, 10, 100, 1000]:
    for gamma in [0.001, 0.0005, 0.0001, 0.00005, 0.00001, 0.000005, 0.000001, 0.0000001, 0.00000001]:

        print(f'Fitting SVC with C: {c_value}, kernel: {kernel}, gamma: {gamma}')

        # Fit Model with Hyperparameter
        clf = svm.SVC(C=c_value, gamma=gamma)
        clf.fit(x_train, y_train)

        # Calculate Score
        predicted = clf.predict(x_test)
        score = f1_score(y_test, predicted, average='macro')

        print(f'Score: {score}')

        # Save results in log file
        with open(log_path, 'a') as f:
            f.write(f'{c_value},{kernel},{gamma},{score}\n')
