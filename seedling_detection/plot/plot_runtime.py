import pandas as pd
import seaborn as sns


# Get log
log_path = 'execute_seedling_detection/runtime_log.txt'
df = pd.read_csv(log_path)
df.Size /= 1000000  # Convert size to mb
df.Runtime /= 60  # Convert Runtime to Minutes

# Create a regression plot with seaborn
ax = sns.regplot(x=df.Size, y=df.Runtime)
ax.set_xlabel('Größer des Orthofotos (GB)')
ax.set_xlim([1.5, 7.5])
ax.set_ylabel('Rechenzeit (Minuten)')
ax.set_title('Einfluss der Dateigröße auf die Rechenzeit')
