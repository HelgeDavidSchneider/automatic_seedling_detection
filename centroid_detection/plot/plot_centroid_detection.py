import numpy as np
import rasterio
import cv2
from rasterio.windows import Window
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max
from segmentation.vi_functions import calculate_RGBVI, normalize_vi
from segmentation.threshold_functions import threshold_otsu_func
from segmentation.image_operations import find_centroids


# Import Orthophoto
ortho_path = 'E:/Ortho/F0173B-1.tif'
ds = rasterio.open(ortho_path)

# Get dynamic window size from the ortho with the shape around 1000 x 1000 pixel
r = 40000
c = 40000

# Take subsample from the moving window
img_sample = np.dstack([ds.read(1, window=Window(c, r, 1000, 1000)),
                        ds.read(2, window=Window(c, r, 1000, 1000)),
                        ds.read(3, window=Window(c, r, 1000, 1000))])

# Get Vegetation mask
RGBVI = normalize_vi(calculate_RGBVI(img_sample))
RGBVI = cv2.GaussianBlur(RGBVI, (3, 3), 0)
RGBVI_otsu = threshold_otsu_func(RGBVI)[0]
RGBVI[~RGBVI_otsu] = 0  # Replace values with threshold

# Find centroids
centroids_geometric = find_centroids(RGBVI_otsu.astype(np.uint8) * 255)
centroids_local_maxima = peak_local_max(RGBVI, min_distance=20)

x1 = [e[0] for e in centroids_geometric]
y1 = [e[1] for e in centroids_geometric]

x2 = [e[0] for e in centroids_local_maxima]
y2 = [e[1] for e in centroids_local_maxima]

# Plot Result
fig, ax = plt.subplots(2, 2)
ax[0, 0].imshow(img_sample)
ax[0, 0].set_title('Luftbild')
ax[0, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[0, 1].imshow(RGBVI, cmap=plt.cm.binary)
ax[0, 1].set_title('RGBVI mit Schwellenwert')
ax[0, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[1, 0].imshow(RGBVI_otsu, cmap=plt.cm.binary)
ax[1, 0].scatter(y1, x1, c='red', marker="x", s=15)
ax[1, 0].set_title('Geometrische Zentren der Segmente')
ax[1, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)


ax[1, 1].imshow(RGBVI, cmap=plt.cm.binary)
ax[1, 1].scatter(y2, x2, c='red', marker="x", s=15)
ax[1, 1].set_title('Lokale Maxima der Segmente')
ax[1, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

plt.tight_layout()
