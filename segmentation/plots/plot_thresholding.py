import numpy as np
import rasterio
from rasterio.windows import Window
import matplotlib.pyplot as plt
from segmentation.vi_functions import calculate_RGBVI, calculate_GLI, calculate_VARI, normalize_vi
from segmentation.threshold_functions import threshold_mean_func, threshold_otsu_func

# Import Orthophoto
ortho_path = 'E:/Ortho/F0173B-1.tif'
ds = rasterio.open(ortho_path)

# Get dynamic window size from the ortho with the shape around 1000 x 1000 pixel
r = 40000
c = 40000

# Take subsample from the moving window
img_sample = np.dstack([ds.read(1, window=Window(c, r, 1000, 1000)),
                        ds.read(2, window=Window(c, r, 1000, 1000)),
                        ds.read(3, window=Window(c, r, 1000, 1000))])

# Get VIs
RGBVI = normalize_vi(calculate_RGBVI(img_sample))
GLI = normalize_vi(calculate_GLI(img_sample))
VARI = normalize_vi(calculate_VARI(img_sample))

# Threshold
RGBVI_mean = threshold_mean_func(RGBVI)[0]
GLI_mean = threshold_mean_func(calculate_GLI(img_sample))[0]
VARI_mean = ~threshold_mean_func(calculate_VARI(img_sample))[0]
RGBVI_otsu = threshold_otsu_func(RGBVI)[0]
GLI_otsu = threshold_otsu_func(calculate_GLI(img_sample))[0]
VARI_otsu = ~threshold_otsu_func(calculate_VARI(img_sample))[0]

# RGBVI
fig, ax = plt.subplots(3, 3)
cmap = 'jet'
cmap_r = 'jet_r'
plt.rcParams.update({'font.size': 14})


ax[0, 0].imshow(RGBVI, cmap=cmap)
ax[0, 0].set_title('RGBVI')
ax[0, 0].set_ylabel('Vegetations Index')
ax[0, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[1, 0].imshow(RGBVI_mean, cmap=plt.cm.binary)
ax[1, 0].set_ylabel('Mittlerer Schwellenwert')
ax[1, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)
ax[2, 0].set_ylabel("Otsu's Methode")
ax[2, 0].imshow(RGBVI_otsu, cmap=plt.cm.binary)
ax[2, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# GLI
ax[0, 1].imshow(GLI, cmap=cmap)
ax[0, 1].set_title('GLI')
ax[0, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[1, 1].imshow(GLI_mean, cmap=plt.cm.binary)
ax[1, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[2, 1].imshow(RGBVI_otsu, cmap=plt.cm.binary)
ax[2, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# VARI
ax[0, 2].imshow(VARI, cmap=cmap_r)
ax[0, 2].set_title('VARI')
ax[0, 2].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[1, 2].imshow(VARI_mean, cmap=plt.cm.binary)
ax[1, 2].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

ax[2, 2].imshow(VARI_otsu, cmap=plt.cm.binary)
ax[2, 2].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

plt.tight_layout()

#AXEN WEG