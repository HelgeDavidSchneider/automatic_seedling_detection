"""
This script includes certain image operations as helper functions.
"""

import numpy as np
import imutils
from PIL import Image, ImageEnhance
import cv2


def normalize_image(img):
    """
    This function normalizes values to the scale of 0:255
    Taken from:
    https://stackoverflow.com/questions/1735025/how-to-normalize-a-numpy-array-to-within-a-certain-range
    :param img: numpy ndarray
    :return:
    """

    img_pos = img - np.min(img)
    img_scaled = img_pos / np.ptp(img)
    img_norm = 255 * img_scaled
    return img_norm.astype(np.uint8)


def calculate_rgbvi(img):
    """
    Calculates the RGBVI Vegetation Index for an image.
    Formula:
    RGBVI = ((G * G) - (R * B)) / (G * G) + (R * B)

    Taken from:
    ttps://stackoverflow.com/questions/929103/convert-a-number-range-to-another-range-maintaining-ratio
    :param img: image as numpy array
    :return:
    """

    # Calculate the RGBVI by the formula
    img = img + 0.00001  # Add infenitesimal small number to avoid zero division error
    gg = img[:, :, 1] * img[:, :, 1]  # g * g
    rb = img[:, :, 0] * img[:, :, 2]  # r * b
    rgbvi = (gg - rb) / (gg + rb)

    return rgbvi


def enhance_image(img, enhance_factor=4):
    """
    Increases the contrast of an image as numpy array
    Taken from
    https://www.kite.com/python/examples/3184/pil-increase-the-contrast-of-an-image

    It seems like it just blends the image with a grayscale.
    More research is needed here if this step is even neccessary
    https://stackoverflow.com/questions/59166448/whats-the-formula-used-in-pil-imageenhance-enhance-feature-for-color-brightnes

    :param img: numpy array with the grayscale image
    :param enhance_factor: factor how much the contrast of the image will be increased.
    :return:
    """

    normalized = normalize_image(img)  # Rescale image to 0:255
    im = Image.fromarray(normalized)
    contrast_enhancer = ImageEnhance.Contrast(im)
    im_enhanced = contrast_enhancer.enhance(enhance_factor)

    return np.array(im_enhanced)


def segment_image(img):
    """
    This function performs image_segmentation by calculating the RGBVI
    :param img: numpy ndarray with the rgb image
    :return: numpy ndarray with the segmented vegetation
    """

    # Calculate RGBVI
    rgbvi = calculate_rgbvi(img)

    # Enhance the RGBVI to increase the contrast
    rgbvi_enh = enhance_image(rgbvi, enhance_factor=4)

    # Create mask from the enhanced RGBVI by all values which are higher than 180
    # ret, thresh_enh = cv2.threshold(rgbvi_enh, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    # ret, thresh_enh = cv2.threshold(rgbvi_enh, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    thresh_enh = np.zeros_like(rgbvi_enh)
    thresh_enh[rgbvi_enh < 160] = 255

    return thresh_enh


def find_centroids(img):
    """
    This function finds the pixel centroids of segmented objects in a grayscale image
    :param img: grayscale image as numpy array
    :return: list of the centroid indexes as pixel coordinates
    """

    # Filter, blur and find edges in the image
    # img_filtered = cv2.bilateralFilter(img, 5, 255, 255)
    # img_blurred = cv2.GaussianBlur(img_filtered, (3, 3), 0)
    # img_median = cv2.medianBlur(img_blurred, 1)
    img_edged = cv2.Canny(img, 30, 200)

    # Find the contours over the edges of the image and print the contours coordinates.
    # Based on reversing order of countour area so we could also reduce noise by filtering out the large countours.
    img_cont = cv2.findContours(img_edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    img_cont = imutils.grab_contours(img_cont)
    img_cont = sorted(img_cont, key=cv2.contourArea, reverse=True)

    # Compute the center of the contour lines
    indexes = []
    for c in img_cont:

        # Compute the m00 of contours to avoid zero division error
        m = cv2.moments(c)
        if m["m00"] > 0:
            cx = int(m["m01"] / m["m00"])
            cy = int(m["m10"] / m["m00"])

            indexes.append((cx, cy))

    return indexes
