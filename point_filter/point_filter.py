"""
This script includes a point filter which is filtering out close by points and keeps most centroid point.
"""

import os
from collections import OrderedDict
import numpy as np
import fiona
from fiona.crs import from_epsg
import rasterio
from shapely.geometry import asShape, Point, mapping
import scipy.spatial as spatial


def point_filter(points, threshold=10):
    """
    This function detects clusters of points and replaces them by their centroid point.
    :param points: List of point coordinates as tuples
    :param threshold: Integer or float with the maximum distance for two points to be considered as cluster.
    :return: List with the coordinates of the filtered points
    """

    # If there are no data points return empty list
    if len(points) == 0:
        return []

    # Create spatial tree from points
    point_tree = spatial.cKDTree(points)

    # Save output and already clustered points in lists
    filtered_points = []
    clustered_points = []

    # Iterate over all points
    for i, point in enumerate(point_tree.data):

        # Check if point has already been assigned to a cluster
        if i in clustered_points:
            continue

        # Get the indexes of all points in the neighbourhood
        neighbours_idx = point_tree.query_ball_point(point, threshold)

        # If there are neighbours get the centroid of the cluster
        if len(neighbours_idx) > 1:

            # Find centroid of the neighbourhood
            neighbourhood = point_tree.data[neighbours_idx]
            centroid = np.mean(neighbourhood, axis=0)

            # Get point with the closest distance to the centroid
            dist_to_centroid = [int(spatial.distance.euclidean(p, centroid)) for p in neighbourhood]
            closest_point = neighbourhood[dist_to_centroid.index(min(dist_to_centroid))]
            filtered_points.append(closest_point)

            # Add all points from the cluster to the already clustered points
            clustered_points += neighbours_idx

        # If there is no neighbourhood just append the point itself
        else:
            filtered_points.append(point)

    # Convert the coordinates to integer values
    filtered_points = [(int(p[0]), int(p[1])) for p in filtered_points]

    return filtered_points


def filter_points_from_shapefile(centroids="output/classified_features/classified_features.shp",
                                 ortho_path=os.path.realpath("C:/Users/Arbeit/PycharmProjects/python-automation/testing/test_data/composite.tif"),
                                 threshold=10,
                                 output_filtered_points="output/classified_features/classified_features_filtered.shp"):
    # Import Ortho
    ds = rasterio.open(ortho_path)

    # Import seeds
    with fiona.open(centroids, 'r') as source:

        # Convert points to shape
        centroid_points = [asShape(f['geometry']).simplify(1)
                           for f in list(source) if f['properties']['vCL'] == 0]

        # Get the pixel index in the ortho array for each point (Neccessary for threshold in pixels)
        centroid_idx = [ds.index(p.x, p.y) for p in centroid_points]

    # Filter the points
    filtered_points = point_filter(centroid_idx, threshold)

    # Transform points back to their CRS
    filtered_points_ref = [Point(ds.xy(p[0], p[1])) for p in filtered_points]

    # Save the filtered points as shapefile
    data_schema = {'geometry': 'Point', 'properties': OrderedDict([('vCL', 'int')])}
    data_crs = fiona.crs.from_epsg(ds.crs.to_epsg())

    with fiona.open(output_filtered_points, 'w', 'ESRI Shapefile', data_schema, crs=data_crs) as output:
        for feat in filtered_points_ref:
            output.write({'properties': OrderedDict([('vCL', 0)]), 'geometry': mapping(feat)})
