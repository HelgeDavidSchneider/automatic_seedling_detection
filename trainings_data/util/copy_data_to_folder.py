"""
This script copies all files in the given sheet to an external storage device
"""

import os
import numpy as np
import pandas as pd
from shutil import copyfile

# Import Trainingsdata sheet
sheet_path = 'create_trainings_data/sheets/abp_eucalyptus.csv'
ds = pd.read_csv(sheet_path, sep=';')
ds = ds.replace(np.nan, '', regex=True)  # Replace nan with empty string

# Append file type extenstions and join paths
ds['Ortho_Name'] = ds['Ortho_Name'] + '.tif'
ds['Train_Name'] = ds['Train_Name'] + '.shp'
ds['ortho_path'] = [os.path.join(dir, file) for dir, file in zip(ds['Ortho_Folder'], ds['Ortho_Name'])]
ds['train_path'] = [os.path.join(dir, file) for dir, file in zip(ds['Train_Folder'], ds['Train_Name'])]

# Target directories
ortho_dir = os.path.abspath('Z:/z_Helge/Ortho')
shape_dir = os.path.abspath('Z:/z_Helge/Shape')

# Copy Orthofotos
for name, ortho, shape in zip(ds['Project Name'], ds['ortho_path'], ds['train_path']):
    if os.path.isfile(ortho):
        pass
        # print(f'Copying: {ortho}, to: {os.path.join(ortho_dir, os.path.basename(ortho))}')
        # copyfile(ortho, os.path.join(ortho_dir, os.path.basename(ortho)))

    if os.path.isfile(shape):
        # print(f'Copying: {shape} to: {os.path.join(shape_dir, os.path.basename(shape))}')

        # Copy all parts of the shapefiles
        original_shape_dir = os.path.dirname(shape)
        for file in os.listdir(original_shape_dir):
            filename = os.path.splitext(os.fsdecode(file))[0]
            shapename = os.path.splitext(os.path.basename(shape))[0]

            if filename == shapename:
                copyfile(os.path.join(original_shape_dir, file),
                         os.path.join(shape_dir, os.path.basename(file)))
