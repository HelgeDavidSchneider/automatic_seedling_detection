"""
Script to check if seedlings are contained in the segments of an image.
"""

import fiona
import numpy as np
from shapely.geometry import asShape
import rasterio
from rasterio.windows import Window


def find_seedlings_in_threshold(thresh_img, shape):

    # Import Orthophoto
    ds = rasterio.open(thresh_img)

    # Import the seedlings
    with fiona.open(shape, 'r') as source:

        # Convert points to shape
        centroid_points = [asShape(f['geometry']).simplify(1)
                           for f in list(source) if f['properties']['vCL'] == 0]

        # Get the pixel index in the ortho array for each point (Neccessary for threshold in pixels)
        try:
            centroid_idx = [ds.index(p.x, p.y) for p in centroid_points]
        except TypeError:
            centroid_idx = []

    # Get the number of contained seedlings
    seedlings_in_thresh = [int(ds.read(1, window=Window(idx[1], idx[0], 1, 1)))
                           for idx in centroid_idx
                           if len(ds.read(1, window=Window(idx[1], idx[0], 1, 1))) == 1]

    return np.sum(seedlings_in_thresh) / 255, len(seedlings_in_thresh)


if __name__ == '__main__':
    find_seedlings_in_threshold(thresh_img='Z:/z_Helge/Projects/F0233B-3_vi_otsu.tif',
                                shape='Z:/z_Helge/Shape/2003_ABP_SeedlingEastAustralia_F0388B_2_TrainingsData_SJ_v1.shp')
