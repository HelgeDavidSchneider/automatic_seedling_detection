"""
Script to compare different threshold methods
https://scikit-image.org/docs/0.13.x/auto_examples/xx_applications/plot_thresholding.html
"""

import numpy as np
import rasterio
from skimage.filters import threshold_mean, threshold_minimum, threshold_otsu


def threshold_fix(img):
    thresh_fix = 160
    binary = img > thresh_fix

    return binary, thresh_fix


def threshold_mean_func(img):
    thresh = threshold_mean(img)
    binary = img > thresh
    return binary, thresh


def threshold_min(img):
    thresh_min = threshold_minimum(img)
    binary = img > thresh_min
    return binary, thresh_min


def threshold_otsu_func(img):
    thresh_otsu = threshold_otsu(img)
    binary = img > thresh_otsu
    return binary, thresh_otsu


def threshold_vi(vi, output, thresh_func):
    """
    Uses a threshold function on a binary image.
    """

    # Import Orthophoto
    ds = rasterio.open(vi)

    # Threshold the image
    vi_img = ds.read(1)
    vi_thresh, thresh = thresh_func(vi_img)
    vi_thresh = vi_thresh.astype(np.uint8) * 255  # convert binary image to pixel
    vi_thresh = np.array([vi_thresh], dtype=np.uint8)

    # Save the Thresholdes image as Geotiff
    with rasterio.Env():

        # Take the profile from the original source and update it
        profile = ds.profile
        profile.update(dtype=rasterio.uint8, count=1)

        with rasterio.open(output, 'w', **profile) as dst:
            dst.write(vi_thresh)

    return thresh


def get_threshold_statistics(threshholded_img):
    """
    Uses a threshold function on a binary image.
    """

    # Import Orthophoto
    ds = rasterio.open(threshholded_img)
    thresh_img = ds.read(1)

    # Create Output Variables to store the intermediate results (vegetation mask and all detected features)
    n_segments = get_number_of_labels(thresh_img.astype(np.bool))
    n_pixel = ds.height * ds.width

    return n_pixel, n_segments


def get_number_of_labels(img):
    """
    This function counts all the labels in a numpy array to avoid memory allocation error in large images.
    :param img: binary image
    :return: the image with labels
    """

    n_rit = int(img.shape[0] / 10000)  # number of row iterations
    n_cit = int(img.shape[1] / 10000)  # number of row iterations

    # Output variable
    n_labels = 0

    # Moving window over the ortho
    for r in np.arange(n_rit) * 10000:
        for c in np.arange(n_cit) * 10000:

            # Take subsample from the moving window
            img_sample = img[r:r + 10000, c:c + 10000]

            # Label the image
            img_sample_labelled = label(img_sample)

            n_labels += img_sample_labelled.max()

    return n_labels


if __name__ == '__main__':
    threshold_vi(vi='C:/Users/Arbeit/Desktop/data/F0233B-3_RGBVI_threshold_otsu_func.tif',
                 output='Z:/z_Helge/Projects/F0233B-3/F0233B-3_RGBVI_threshold_otsu.tif',
                 thresh_func=threshold_otsu_func)

    n_pixel, n_segments = get_threshold_statistics(threshholded_img='C:/Users/Arbeit/Desktop/data/F0233B-3_RGBVI_threshold_otsu_func.tif')
