"""
This script performs peak detection on an orthophoto.
First it calculated the RGBVI and then it uses the peak_local_max search.
The peaks are then saved as georeferenced points to a shapefile.
"""

import numpy as np
import fiona
from fiona.crs import from_epsg
from collections import OrderedDict
import rasterio
from rasterio.windows import Window
from shapely.geometry import Point, mapping
from segmentation.image_operations import calculate_rgbvi, normalize_image
from skimage.feature import peak_local_max
import cv2


def detect_vegetation_peaks(ortho_path='Path/to/ortho',
                            peak_distance=10,
                            output_rgbvi_path='output/rgbvi/rgbvi.tif',
                            output_objects_path='output/detected_features/detected_features.shp'):
    """
    Detects the vegetation peaks on a RGB Tiff file.
    First it calculates the RGBVI. Then it performs blurring, thresholding and local maxima to detect the peaks.
    :param ortho_path: String with the path to the Orthophoto
    :param peak_distance: Minimum number of pixels between two peaks.
    :param output_rgbvi_path: String with the path where the RGBVI will be saved as an intermediate result.
    :param output_objects_path: String with the path where the detected peaks will be saved as shapefiles.
    :return:
    """

    # Import Orthophoto
    ds = rasterio.open(ortho_path)

    # Get dynamic window size from the ortho with the shape around 1000 x 1000 pixel
    pad = 10  # Padding which will be applied to each moving window
    n_rit = int(ds.height / 1000)  # number of row iterations
    rstep = int(ds.height / n_rit)  # row window size
    n_cit = int(ds.width / 1000)  # number of row iterations
    cstep = int(ds.width / n_cit)  # column window size

    # Create Output Variables to store the intermediate results (vegetation mask and all detected features)
    ortho_rgbvi = np.memmap('centroid_detection/rgbvi_memap.dat', dtype=np.uint8, mode='w+', shape=(ds.height, ds.width))
    detected_features = []

    # Moving window over the ortho
    for r in np.arange(n_rit) * rstep:
        for c in np.arange(n_cit) * cstep:

            # Take subsample from the moving window
            img_sample = np.dstack([ds.read(1, window=Window(c, r, cstep + pad, rstep + pad)),
                                    ds.read(2, window=Window(c, r, cstep + pad, rstep + pad)),
                                    ds.read(3, window=Window(c, r, cstep + pad, rstep + pad))])

            # Calculate RGBVI and filter out non vegetation pixel by a threshold
            rgbvi = calculate_rgbvi(img_sample)

            # Blur it to remove noise
            # img_filtered = cv2.bilateralFilter(img, 5, 255, 255)
            # img_median = cv2.medianBlur(img_blurred, 1)
            rgbvi = cv2.GaussianBlur(rgbvi, (3, 3), 0)

            # Threshold the RGBVI
            rgbvi[rgbvi < 0.1] = -1

            # Peak detection
            coordinates = peak_local_max(rgbvi, min_distance=peak_distance)

            # Save the intermediate results
            detected_features.extend([Point(ds.xy(p[0]+ r, p[1] + c)) for p in coordinates])

            rgbvi_norm = normalize_image(rgbvi)
            ortho_rgbvi[r:r + rstep + pad, c:c + cstep + pad] = rgbvi_norm

    # Save the detected features with fiona
    data_schema = {'geometry': 'Point', 'properties': OrderedDict([])}
    data_crs = fiona.crs.from_epsg(ds.crs.to_epsg())

    with fiona.open(output_objects_path, 'w', 'ESRI Shapefile', data_schema, crs=data_crs) as output:
        for feat in detected_features:
            output.write({
                'properties': OrderedDict([]),
                'geometry': mapping(feat)
            })

    # Save the Vegetation Mask as Geotiff
    with rasterio.Env():

        # Take the profile from the original source and update it
        profile = ds.profile
        profile.update(dtype=rasterio.uint8, count=1)

        with rasterio.open(output_rgbvi_path, 'w', **profile) as dst:
            dst.write(ortho_rgbvi.astype(rasterio.uint8), 1)


if __name__ == '__main__':

    detect_vegetation_peaks(ortho_path='Path/to/ortho',
                            peak_distance=10,
                            output_rgbvi_path='output/rgbvi/rgbvi.tif',
                            output_objects_path='output/detected_features/detected_features.shp')
