import numpy as np
import rasterio
from rasterio.windows import Window
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
from segmentation.vi_functions import calculate_RGBVI, calculate_GLI, calculate_NGRDI, calculate_VARI, normalize_vi

# Import Orthophoto
ortho_path = 'E://Ortho/F0173B-1.tif'
ds = rasterio.open(ortho_path)

# Get dynamic window size from the ortho with the shape around 1000 x 1000 pixel
r = 40000
c = 40000

# Take subsample from the moving window
img_sample = np.dstack([ds.read(1, window=Window(c, r, 1000, 1000)),
                        ds.read(2, window=Window(c, r, 1000, 1000)),
                        ds.read(3, window=Window(c, r, 1000, 1000))])


# Get VIs
RGBVI = normalize_vi(calculate_RGBVI(img_sample))
GLI = normalize_vi(calculate_GLI(img_sample))
NGRDI = normalize_vi(calculate_NGRDI(img_sample))
VARI = normalize_vi(calculate_VARI(img_sample))

# Compare the different vegetation indexes
fig, ax = plt.subplots(2, 3)
cmap = 'jet'
cmap_r = 'jet_r'
plt.rcParams.update({'font.size': 14})
plt.suptitle('Vergleich der Vegetationsindizes', fontsize=16)


ax[0, 0].imshow(img_sample, cmap=cmap)
ax[0, 0].set_title('Original')
ax[0, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# RGB Grayscale
ax[0, 1].imshow(rgb2gray(img_sample), cmap=plt.cm.binary_r)
ax[0, 1].set_title('Graustufen')
ax[0, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# NGRDI
ax[0, 2].imshow(NGRDI, cmap=cmap_r)
ax[0, 2].set_title('NGRDI')
ax[0, 2].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# VARI
ax[1, 0].imshow(VARI, cmap=cmap_r)
ax[1, 0].set_title('VARI')
ax[1, 0].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# GLI
ax[1, 1].imshow(GLI, cmap=cmap)
ax[1, 1].set_title('GLI')
ax[1, 1].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

# RGBVI
ax[1, 2].imshow(RGBVI, cmap=cmap)
ax[1, 2].set_title('RGBVI')
ax[1, 2].tick_params(axis='both', which='both', bottom=False, top=False,
                     labelbottom=False, right=False, left=False, labelleft=False)

plt.tight_layout()
