"""
This script performs object classification on a list of features for a trained ml model.
"""

import os
import joblib
from collections import OrderedDict
import numpy as np
import fiona
from shapely.geometry import asShape, mapping
import rasterio
from rasterio.windows import Window
from shapely.geometry import Point


def classify_objects(ds,
                     candidates,
                     radius=3,
                     model='classifier_model/object_unmixing_classifier.joblib.pkl'):
    """
    This function classifies candidate pixels on an image with the given model
    :param ds: Rasterio Ortho Object in .tif format
    :param candidates: List of tuples with the index of the candidate pixels
    :param radius: integer or float with the radius of the classified images
    :param model: String with the path to the scikit learn model packed with joblib.
    :return: numpy array with the classified candidates
    """

    # If there are no features in the window return an empty array
    if len(candidates) == 0:
        return [], [], []

    # Create Array with images of the detected features
    diameter = 2 * radius + 1
    flat_len = diameter ** 2 * 3  # Flattened length of the images is Height ** 2 times the number of bands
    images = np.empty(shape=(len(candidates), flat_len), dtype=np.uint8)

    for i, p in enumerate(candidates):

        # Read image sample directly from tif file
        x, y = candidates[i]
        img_sample = np.dstack([ds.read(1, window=Window(y, x, diameter, diameter)),
                                ds.read(2, window=Window(y, x, diameter, diameter)),
                                ds.read(3, window=Window(y, x, diameter, diameter))])

        # Flatten the image, if it is from the border take it as just zero values
        if img_sample.shape == (diameter, diameter, 3):
            images[i] = img_sample.flatten()
        else:
            images[i] = np.zeros(shape=(1, flat_len))

    # Classify the data (Predict seedling, weed or ground for every image sample)
    classifier = joblib.load(model)
    predicted = classifier.predict(images)

    return predicted


def classify_objects_from_shapefile(ortho_path='path/to/ortho',
                                    object_centroids='path/to/centroids',
                                    radius=3,
                                    ml_model='classifier_model/svc_eucalyptus_abp.joblib.pkl',
                                    output_path="output/classified_features/classified_features.shp"):
    """
    This function classifies points on an orthophoto and saves the classified points as shapefile
    :param ortho_path: String with the path to the orthophoto in .tif format
    :param object_centroids: The points on the orthophoto which shall be classified as shapefile
    :param radius: Integer with the radius of objects in pixels. Needs to be adjusted when another model is chosen.
    :param ml_model: The ML model which should be used for the classification. Packed with joblib.
    :param output_path: The path where the classified points should be stored.
    :return:
    """

    # Import Ortho
    ds = rasterio.open(ortho_path)

    with fiona.open(object_centroids, 'r') as source:

        # Convert points to shape
        centroid_points = [asShape(f['geometry']).simplify(1) for f in list(source)]

        # Get the pixel index in the ortho array for each point
        centroids = [ds.index(p.x, p.y) for p in centroid_points]

    # Classify the centroids
    pred = classify_objects(ds=ds,
                            candidates=centroids,
                            radius=radius,
                            model=ml_model)

    seed = [p for i, p in enumerate(centroids) if pred[i] == 0]
    ground = [p for i, p in enumerate(centroids) if pred[i] == 1]
    weed = [p for i, p in enumerate(centroids) if pred[i] == 2]

    # Transform the detected points back to their CRS and save them in the output variables
    detected_seeds = [Point(ds.xy(s[0], s[1])) for s in seed]
    detected_ground = [Point(ds.xy(g[0], g[1])) for g in ground]
    detected_weeds = [Point(ds.xy(w[0], w[1])) for w in weed]

    # Save the classified points to a shapefile
    data_schema = {'geometry': 'Point', 'properties': OrderedDict([('vCL', 'int')])}
    data_crs = fiona.crs.from_epsg(ds.crs.to_epsg())

    with fiona.open(output_path, 'w', 'ESRI Shapefile', data_schema, crs=data_crs) as output:
        for feat in detected_seeds:
            output.write({'properties': OrderedDict([('vCL', 0)]), 'geometry': mapping(feat)})
        for feat in detected_ground:
            output.write({'properties': OrderedDict([('vCL', 1)]), 'geometry': mapping(feat)})
        for feat in detected_weeds:
            output.write({'properties': OrderedDict([('vCL', 2)]), 'geometry': mapping(feat)})


if __name__ == '__main__':

    op = os.path.realpath("Z:/KEY ACCOUNTS/ABP_AUS/2003_ABP_SeedlingEastAustralia/xF0357C/3_ORTHO/F0357C_Ortho.tif")
    cp = os.path.realpath("Z:/KEY ACCOUNTS/ABP_AUS/2003_ABP_SeedlingEastAustralia/xF0357C/4_GIS/FirstTry_PointsHelgeSaran/Points_v1_R4.shp")
    classify_objects_from_shapefile(ortho_path=op,
                                    object_centroids=cp,
                                    radius=3,
                                    ml_model='classifier_model/svc_eucalyptus_abp.joblib.pkl',
                                    output_path="output/classified_features/classified_features.shp")
