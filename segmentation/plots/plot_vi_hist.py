import numpy as np
import rasterio
from rasterio.windows import Window
import matplotlib.pyplot as plt
from segmentation.vi_functions import calculate_RGBVI, calculate_GLI, calculate_NGRDI, calculate_VARI, normalize_vi

# Import Orthophoto
ortho_path = 'E:/Ortho/F0173B-1.tif'
ds = rasterio.open(ortho_path)

# Get dynamic window size from the ortho with the shape around 1000 x 1000 pixel
r = 40000
c = 40000

# Take subsample from the moving window
img_sample = np.dstack([ds.read(1, window=Window(c, r, 1000, 1000)),
                        ds.read(2, window=Window(c, r, 1000, 1000)),
                        ds.read(3, window=Window(c, r, 1000, 1000))])


# Get VIs
RGBVI = normalize_vi(calculate_RGBVI(img_sample)).flatten()
GLI = normalize_vi(calculate_GLI(img_sample)).flatten()
NGRDI = normalize_vi(calculate_NGRDI(img_sample)).flatten()
VARI = normalize_vi(calculate_VARI(img_sample)).flatten()

# NGRDI
fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
ax[0, 0].hist(NGRDI, bins=1000, histtype='step')
ax[0, 0].set_title('NGRDI')

# VARI
ax[0, 1].hist(VARI, bins=1000, histtype='step')
ax[0, 1].set_title('VARI')

# GLi
ax[1, 0].hist(GLI, bins=1000, histtype='step')
ax[1, 0].set_title('GLI')

# RGBVI
ax[1, 1].hist(RGBVI, bins=1000, histtype='step')
ax[1, 1].set_title('RGBVI')

fig.suptitle("Werteverteilung der Vegetationsindizes für einen Bildausschnitt", fontsize=14)
fig.text(0.5, 0.04, 'VI Wert', ha='center')
fig.text(0.04, 0.5, 'Anzahl Pixel', va='center', rotation='vertical')
