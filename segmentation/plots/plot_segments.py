import pandas as pd
import matplotlib.pyplot as plt

# Import number of seedlings pixels per threshold and VI
dfs = pd.read_csv('segmentation/log_edit.txt')
dfs['Seed Percentage'] = dfs['Number of Seeds'] / dfs['Total Seeds']
dfs['Segments per Area'] = dfs['Number of Segments'] / dfs['Number of Pixel'] * 1000000

# Import Threshold values
dft = pd.read_csv('segmentation/log_thresh_edit.txt', usecols=range(1, 5))
dft = dft.drop(dft[dft['Threshold Function'] == 'threshold_fix'].index)
dfs = dfs.drop(dfs[dfs['Threshold Function'] == 'threshold_fix'].index)

# Plot Number of contained Pixel
fig, axes = plt.subplots(3, 2)  # create figure and axes
ax = axes.flatten()
dfs[(dfs['Vi Name'] == 'RGBVI')][['Vi Name', 'Threshold Function', 'Seed Percentage']].boxplot(by='Threshold Function',
                                                                                               ax=ax[0],
                                                                                               notch=False,
                                                                                               patch_artist=True,
                                                                                               bootstrap=10000,
                                                                                               widths=0.6)
ax[0].tick_params(axis='x', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
ax[0].title.set_text('RGBVI')
ax[0].set_ylabel('Anteil enthaltener Pixel')

dfs[(dfs['Vi Name'] == 'GLI')][['Vi Name', 'Threshold Function', 'Seed Percentage']].boxplot(by='Threshold Function',
                                                                                             ax=ax[1],
                                                                                             notch=False,
                                                                                             patch_artist=True,
                                                                                             bootstrap=10000,
                                                                                             widths=0.6)
ax[1].title.set_text('GLI')

ax[1].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)

# Plot Number of Segments per VI and Threshold
dfs[dfs['Vi Name'] == 'RGBVI'][['Vi Name', 'Threshold Function', 'Segments per Area']].boxplot(by='Threshold Function',
                                                                                               ax=ax[2],
                                                                                               notch=False,
                                                                                               patch_artist=True,
                                                                                               bootstrap=10000,
                                                                                               widths=0.6)
ax[2].tick_params(axis='x', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
ax[2].title.set_text('')
ax[2].set_ylabel('Segmente / Mio. Pixel')

dfs[dfs['Vi Name'] == 'GLI'][['Vi Name', 'Threshold Function', 'Segments per Area']].boxplot(by='Threshold Function',
                                                                                             ax=ax[3],
                                                                                             notch=False,
                                                                                             patch_artist=True,
                                                                                             bootstrap=10000,
                                                                                             widths=0.6)
ax[3].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
ax[3].title.set_text('')

# Plot Threshold values
dft[dft['vi'] == 'RGBVI'].boxplot(by='Threshold Function',
                                  ax=ax[4],
                                  notch=False,
                                  patch_artist=True,
                                  bootstrap=10000,
                                  widths=0.6)
ax[4].title.set_text('')
ax[4].set_ylabel('Absoluter Threshold Wert')

dft[dft['vi'] == 'GLI'].boxplot(by='Threshold Function',
                                ax=ax[5],
                                notch=False,
                                patch_artist=True,
                                bootstrap=10000,
                                widths=0.6)
ax[5].tick_params(axis='y', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
ax[5].title.set_text('')


for i in range(len(ax)):
    ax[i].set_xlabel('')
    ax[i].set_xticklabels(['', ''])

for i in range(4, 6):
    ax[i].set_xticklabels(['Mean', 'Otsu'])

plt.suptitle("Vergleich der Vegetationsmasken", fontsize=14)

plt.tight_layout()
