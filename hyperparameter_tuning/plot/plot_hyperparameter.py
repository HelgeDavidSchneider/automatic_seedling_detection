import pandas as pd
import matplotlib.pyplot as plt


# Get log
log_path = 'hyperparameter_tuning/hyperparameter.txt'
df = pd.read_csv(log_path)

plt.rcParams.update({'font.size': 22})


# Plot which shows that Linear Kernel has less params
fig, ax = plt.subplots()
ax.plot(df[df.C == 1].Gamma, df[df.C == 1].Score, label='C 1')
ax.plot(df[df.C == 10].Gamma, df[df.C == 10].Score, label='C 10')
ax.plot(df[df.C == 100].Gamma, df[df.C == 100].Score, label='C 100')
ax.plot(df[df.C == 1000].Gamma, df[df.C == 1000].Score, label='C 1000')
ax.set_xlim(df.Gamma.max(), df.Gamma.min())
ax.set_xlabel('Gamma')
ax.set_ylabel('F1 Score')
ax.set_xscale('log')
ax.legend()
