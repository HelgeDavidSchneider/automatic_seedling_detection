import pandas as pd
import matplotlib.pyplot as plt

# Import number of seedlings pixels per threshold and VI
dfs = pd.read_csv('segmentation/log_edit.txt')
dfs['Seed Percentage'] = dfs['Number of Seeds'] / dfs['Total Seeds']
dfs['Segments per Area'] = dfs['Number of Segments'] / dfs['Number of Pixel'] * 1000000
dfs.loc[dfs['Vi Name'] == 'VARI', 'Seed Percentage'] = \
    (dfs[(dfs['Vi Name'] == 'VARI')]['Seed Percentage'] * - 1) + 1

# Import Threshold values
dft = pd.read_csv('segmentation/log_thresh_edit.txt', usecols=range(1, 5))
dft = dft.drop(dft[dft['Threshold Function'] == 'threshold_fix'].index)
dfs = dfs.drop(dfs[dfs['Threshold Function'] == 'threshold_fix'].index)

dft = dft.replace('threshold_mean_func', 'Mittelwert')
dft = dft.replace('threshold_otsu_func', 'Otsu')

# Plot Number of contained Pixel
fig, axes = plt.subplots(3, 3, sharex='col', sharey='row')  # create figure and axes
ax = axes.flatten()
dfs[(dfs['Vi Name'] == 'RGBVI')][['Vi Name', 'Threshold Function', 'Seed Percentage']].boxplot(by='Threshold Function',
                                                                                               color='blue',
                                                                                               ax=ax[0])

ax[0].tick_params(axis='x', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
ax[0].title.set_text('RGBVI')

dfs[(dfs['Vi Name'] == 'GLI')][['Vi Name', 'Threshold Function', 'Seed Percentage']].boxplot(by='Threshold Function',
                                                                                             color='blue',
                                                                                             ax=ax[1])
ax[1].title.set_text('GLI')

ax[1].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)

dfs[(dfs['Vi Name'] == 'VARI')][['Vi Name', 'Threshold Function', 'Seed Percentage']].boxplot(by='Threshold Function',
                                                                                              color='blue',
                                                                                              ax=ax[2])
ax[2].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
ax[2].title.set_text('VARI')

# Plot Number of Segments per VI and Threshold
dfs[dfs['Vi Name'] == 'RGBVI'][['Vi Name', 'Threshold Function', 'Segments per Area']].boxplot(by='Threshold Function',
                                                                                               color='blue',
                                                                                               ax=ax[3])

ax[4].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
dfs[dfs['Vi Name'] == 'GLI'][['Vi Name', 'Threshold Function', 'Segments per Area']].boxplot(by='Threshold Function',
                                                                                             color='blue',
                                                                                             ax=ax[4])
ax[4].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)

dfs[dfs['Vi Name'] == 'VARI'][['Vi Name', 'Threshold Function', 'Segments per Area']].boxplot(by='Threshold Function',
                                                                                              color='blue',
                                                                                              ax=ax[5])
ax[5].tick_params(axis='both', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)

# Plot Threshold values
dft[dft['vi'] == 'RGBVI'].boxplot(by='Threshold Function', color='blue', ax=ax[6])

dft[dft['vi'] == 'GLI'].boxplot(by='Threshold Function', color='blue', ax=ax[7])
ax[7].tick_params(axis='y', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)
dft[dft['vi'] == 'VARI'].boxplot(by='Threshold Function', color='blue', ax=ax[8])
ax[8].tick_params(axis='y', which='both', bottom=False, top=False,
                  labelbottom=False, right=False, left=False, labelleft=False)

for i in range(len(ax)):
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')

for i in range(3, 9):
    ax[i].title.set_text('')


for i, j in zip(range(0, 9, 3), ['Anteil Referenzpixel (%)', 'Segmente / Mio. Pixel',
                                 'Absoluter Schwellenwert']):
    ax[i].set_ylabel(j)

# plt.suptitle("Statisiken der Vegetationsmasken", fontsize=14)
fig.suptitle('')

plt.tight_layout()
