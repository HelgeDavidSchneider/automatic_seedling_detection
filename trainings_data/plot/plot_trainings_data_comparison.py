import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 22})


ds = [0.92, 0.61, 0.74, 0.85, 0.63]


fig, ax = plt.subplots()
ax.boxplot(ds, widths=0.4, patch_artist=True)
ax.set_xlabel('')
ax.set_xticklabels(['Trainingsdaten'])
ax.set_ylabel(r'Überlappung (%)', fontsize=25)
ax.set_ylim(0, max(ds))
ax.grid(True)
