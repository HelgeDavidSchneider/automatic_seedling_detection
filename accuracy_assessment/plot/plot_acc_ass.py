import pandas as pd
import matplotlib.pyplot as plt


# Get log
log_path = 'accuracy_assessment/acc_log.txt'
df = pd.read_csv(log_path)

# Plot Overall Accuracy
fig, ax = plt.subplots(1, 2, sharey=True)
plt.rcParams.update({'font.size': 22})

df[['Detected Ref Points', 'Double Classified Ref Points', 'Missed Ref Points']].boxplot(ax=ax[0], widths=0.6,
                                                                                         patch_artist=True)
ax[0].set_title("Referenzpunkte")
ax[0].set_xticklabels(["Korrekte\nErkennung", "Mehrfache\nErkennung", "Keine\nErkennung"])
ax[0].set_ylim([0., 1.])
ax[0].set_ylabel('Anteil Setzlinge')

df[['Correct Points', 'Double Classified Points', 'False Points']].boxplot(ax=ax[1], widths=0.6, patch_artist=True)
ax[1].set_title("Lokalisierte Punkte")
ax[1].set_xticklabels(["Korrekte\nErkennung", "Mehrfache\nErkennung", "Keine\nErkennung"])
ax[1].set_ylim([-0.1, 1.1])
ax[1].set_ylabel('')
