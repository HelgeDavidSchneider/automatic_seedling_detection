"""
This script iterates over all multiple orthofotos,
creates segments and checks if data is contained in the segments.
"""

import os
import numpy as np
import pandas as pd
from segmentation.vi_functions import create_vegetation_masks, \
    calculate_RGBVI, calculate_GLI, calculate_VARI, calculate_NGRDI
from segmentation.threshold_functions import \
    threshold_mean_func, threshold_fix, threshold_otsu_func, threshold_vi, get_threshold_statistics
from segmentation.check_if_seedlings_contained_in_segments import find_seedlings_in_threshold

# Import Trainingsdata sheet
sheet_path = 'create_trainings_data/sheets/abp_eucalyptus_with_stats.csv'
ds = pd.read_csv(sheet_path, sep=';')
ds = ds.replace(np.nan, '', regex=True)  # Replace nan with empty string

# Define target directories
ortho_dir = os.path.abspath('E:/Ortho')
shape_dir = os.path.abspath('E:/Shape')
projects_dir = os.path.abspath('E:/Projects')

# Join files with target directories
ds['ortho_path'] = [os.path.join(ortho_dir, file) for file in ds['Ortho_Name']]
ds['train_path'] = [os.path.join(shape_dir, file) for file in ds['Train_Name']]

# Store the results in a dictionary
project_results = {}

# Iterate over projects
with open('segmentation/log.txt', 'a') as f:
    f.write('Project Name,Vi Name,Threshold Function,Number of Pixel,Number of Segments,Number of Seeds,Total Seeds\n')
with open('segmentation/log_thresh.txt', 'a') as f2:
    f2.write('Project Name,Threshold Function,Threshold\n')

for ortho, shape, size in zip(ds['ortho_path'], ds['train_path'], ds['size']):

    # Get project name from ortho
    project_name = os.path.splitext(os.path.basename(ortho))[0]
    print(f'Project {project_name}')

    # Skip projects which are higher than 3.5GB
    if size > 3500000:
        continue

    # Create directory for the project
    project_dir = os.path.join(projects_dir, os.path.splitext(os.path.basename(ortho))[0])
    if not os.path.exists(project_dir):
        os.mkdir(project_dir)

    # Create different VIs for the project
    for vi_func, vi_name in zip([calculate_RGBVI, calculate_GLI, calculate_VARI, calculate_NGRDI],
                                ['RGBVI', 'GLI', 'VARI']):

        # Create veg mask
        veg_mask_path = os.path.join(project_dir,
                                     f'{project_name}_{vi_name}.tif')
        if not os.path.exists(veg_mask_path):
            print(f'Project {project_name} {vi_name}')
            create_vegetation_masks(ortho=ortho,
                                    output=veg_mask_path,
                                    index_func=vi_func)

        # Calculate thresholds for the vegetation mask
        for thresh_func in [threshold_mean_func, threshold_fix, threshold_otsu_func]:

            # Threshold the veg mask
            thresh_mask_path = os.path.join(project_dir, f'{project_name}_{vi_name}_{thresh_func.__name__}.tif')
            if not os.path.exists(thresh_mask_path):
                print(f'Project {project_name} {vi_name} {thresh_func}')
                thresh = threshold_vi(vi=veg_mask_path,
                                      output=thresh_mask_path,
                                      thresh_func=thresh_func)
                with open('segmentation/log_thresh.txt', 'a') as f2:
                    f2.write(f'{project_name},{thresh_func.__name__},{thresh}\n')

            # Get statistics about the segments
            n_pixel, n_segments = get_threshold_statistics(
                threshholded_img=thresh_mask_path)

            # Get statistics about the seedlings in the threshold
            nseeds, nseeds_total = find_seedlings_in_threshold(thresh_img=thresh_mask_path,
                                                               shape=shape)

            # Log the results in a text file
            with open('segmentation/log.txt', 'a') as f:
                f.write(f'{project_name},{vi_name},{thresh_func.__name__},{n_pixel},{n_segments},{nseeds},{nseeds_total}\n')
