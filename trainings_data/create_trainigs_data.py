"""
This script extracts small seedling, weed and ground images with a specific format from an orthophoto.
All images are saved in the trainings_data folder and can be used for the training of a ml model.
"""

import os
import numpy as np
import fiona
import rasterio
from rasterio.windows import Window
import random
from shapely.geometry import asShape
from PIL import Image


def extract_img_samples(ortho='path_to_img',
                        trainingsdata='path_to_point_loc',
                        radius=3,
                        output_dir='path/to/output/dir',
                        seedling_type='eucalyptus',
                        augmentation=True,
                        n_max=1000):
    """
    Extracts seedling, weed and ground samples from an orthophoto and saves them as png images.
    The point data is classified into the groups 'seedling', 'weed' and 'other' based on the attribute 'vCL'.
    Image and Point files need to be in the same crs.
    :param ortho: Path to the Ortho image in Geotiff format (string)
    :param trainingsdata: Path to a shapefile with the point locations.
    Points must have the attribute 'vCL' (string) to be recognized as seedling, weed or ground.
    :param radius: Size of the image radius from the point locations (int)
    :param output_dir: String with path to the output directory
    :param seedling_type: String with the type of seedling. Must be 'pine' or 'eucalyptus'.
    :param augmentation: If true every trainings image will be rotated from 4 directions.
    :param n_max: Integer with the maximum number of samples for the project. Random points will be drawn.
    :return:
    """

    # Check if the seedling_type is specified correct
    if seedling_type not in ['pine', 'eucalyptus']:
        raise ValueError(f'{seedling_type} is not known.\nMust be "pine" or "eucalyptus".')

    # Import Orthophoto
    ds = rasterio.open(ortho)
    ortho_name = os.path.splitext(os.path.basename(ortho))[0]

    # Import Point Locations of different classes from shapefile
    with fiona.open(trainingsdata, 'r') as source:

        # Extract point data based on attribute
        seed = [asShape(f['geometry']).simplify(1) for f in list(source) if np.float(f['properties']['vCL']) == 0.0]
        ground = [asShape(f['geometry']).simplify(1) for f in list(source) if np.float(f['properties']['vCL']) == 1.0]
        weed = [asShape(f['geometry']).simplify(1) for f in list(source) if np.float(f['properties']['vCL']) == 2.0]

    # Convert them to shapely geometrys
    seed = [f for f in seed if f.type == 'Point' and f.is_valid]
    ground = [f for f in ground if f.type == 'Point' and f.is_valid]
    weed = [f for f in weed if f.type == 'Point' and f.is_valid]

    # Check number of seeds
    if len(seed) == 0:
        print(f'Warning! {trainingsdata} does not contain any seedlings.')

    # Only take the maximum number of samples from the trainings data
    if len(seed) > n_max:
        seed = random.sample(seed, n_max)
    if len(ground) > n_max:
        ground = random.sample(ground, n_max)
    if len(weed) > n_max:
        weed = random.sample(weed, n_max)

    # Create directories if they don't exist
    for dir in [seedling_type, 'ground', 'weed']:
        target_dir = os.path.join(output_dir, dir)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)

    # Save image sample of each seed
    diameter = 2 * radius + 1

    for i, p in enumerate(seed):
        x, y = ds.index(p.x, p.y)
        img_sample = ds.read(window=Window(y - radius, x - radius, diameter, diameter))
        img_sample = np.dstack([img_sample[0], img_sample[1], img_sample[2]])  # Transform axes
        img_sample = img_sample.astype(np.uint8)
        if img_sample.shape[0] == diameter and img_sample.shape[1] == diameter:
            im_sample = Image.fromarray(img_sample, mode='RGB')
            im_sample.save(f'{output_dir}/{seedling_type}/{ortho_name}_{seedling_type}_{i}.png')
            if augmentation is True:
                for angle in [90, 180, 270]:
                    image_rotated = im_sample.rotate(angle)
                    image_rotated.save(f'{output_dir}/{seedling_type}/{ortho_name}_{seedling_type}_{i}_rotated_{angle}.png')

    for i, p in enumerate(ground):
        x, y = ds.index(p.x, p.y)
        img_sample = ds.read(window=Window(y - radius, x - radius, diameter, diameter))
        img_sample = np.dstack([img_sample[0], img_sample[1], img_sample[2]])  # Transform axes
        img_sample = img_sample.astype(np.uint8)
        if img_sample.shape[0] == diameter and img_sample.shape[1] == diameter:
            im_sample = Image.fromarray(img_sample, mode='RGB')
            im_sample.save(f'{output_dir}/ground/{ortho_name}_ground_{i}.png')
            if augmentation is True:
                for angle in [90, 180, 270]:
                    image_rotated = im_sample.rotate(angle)
                    image_rotated.save(f'{output_dir}/ground/{ortho_name}_ground_{i}_rotated_{angle}.png')

    for i, p in enumerate(weed):
        x, y = ds.index(p.x, p.y)
        img_sample = ds.read(window=Window(y - radius, x - radius, diameter, diameter))
        img_sample = np.dstack([img_sample[0], img_sample[1], img_sample[2]])  # Transform axes
        img_sample = img_sample.astype(np.uint8)
        if img_sample.shape[0] == diameter and img_sample.shape[1] == diameter:
            im_sample = Image.fromarray(img_sample, mode='RGB')
            im_sample.save(f'{output_dir}/weed/{ortho_name}_weed_{i}.png')
            if augmentation is True:
                for angle in [90, 180, 270]:
                    image_rotated = im_sample.rotate(angle)
                    image_rotated.save(f'{output_dir}/weed/{ortho_name}_weed_{i}_rotated_{angle}.png')

    return


# Helper function to read the sample directory
def get_array_from_samples(img_dir, n_max=1000000):
    """
    This function iterates over all .png images in a directory and stacks them into a numpy array
    :param img_dir: path to the directory with the image samples
    :param n_max: Integer or float with the maximum number of samples. It will draw a random number of samples.
    :return:
    """

    # Load all seedling image samples. Omit the alpha channel
    images = [np.asarray(Image.open(f'{img_dir}/{img}'))[:, :, :3]
              for img in os.listdir(img_dir)]

    if len(images) > n_max:
        images = random.sample(images, n_max)

    # Flatten the images and stack them to a numpy array
    images_flat = [img.flatten() for img in images]

    # Concatenate all seedlings into a numpy array
    images_stacked = np.stack(images_flat, axis=0, out=None)

    return images_stacked


def clear_img_repo(image_dir="path/to/img/dir"):
    """
    This function deletes all created samples in the folder trainings_data/seed, trainings_data/weed and trainings_data/ground
    :param image_dir: Path to the directory where the trainings data is stored
    :return:
    """

    for data_dir in (os.path.join(image_dir, 'eucalyptus'),
                     os.path.join(image_dir, 'pine'),
                     os.path.join(image_dir, 'ground'),
                     os.path.join(image_dir, 'weed')):
        for f in os.listdir(data_dir):
            os.remove(os.path.join(data_dir, f))


# Usage Example
if __name__ == '__main__':

    # clear_img_repo(image_dir=os.getcwd())  # uncomment to delete all trainingsdata
    extract_img_samples(ortho='Z:/PROJEKTE/2020_Projekte/2009_Snowyforests/3_Rohdaten/PART_CPO2survivalnorth_Orthomosaic_export_ThuAug13061322241777_55S.tif',
                        trainingsdata='Z:/PROJEKTE/2020_Projekte/2009_Snowyforests/5_GIS/Trainingsdaten/2009_Snowyforests_Trainingsdata_AG_v1.shp',
                        radius=7,
                        output_dir=os.path.normpath('Z:/METHODIK/Machine_Learning_Models/Trainings_Data/GTFP_Pine_V12'),
                        seedling_type='pine',
                        augmentation=True,
                        n_max=100)
