"""
This script executes the seedling recognition on an orthogoto with an already trained ml model on a list of projects.
"""

import os
import datetime
import numpy as np
import pandas as pd
from seedling_detection.recognize_seedlings import recognize_seedlings


# Import Trainingsdata sheet
sheet_path = 'create_trainings_data/sheets/abp_eucalyptus_with_stats2.csv'
ds = pd.read_csv(sheet_path, sep=',')
ds = ds.replace(np.nan, '', regex=True)  # Replace nan with empty string

# Define target directories
ortho_dir = os.path.abspath('Z:/z_helge/Ortho')
shape_dir = os.path.abspath('Z:/z_helge/Shape')
results_dir = os.path.abspath('Z:/z_helge/Results')
ml_model = os.path.abspath('Z:/z_Helge/Models/svc_eucalyptus_gtfp_7px_radius.joblib.pkl')

# Join files with target directories
ds['ortho_path'] = [os.path.join(ortho_dir, file) for file in ds['Ortho_Name']]
ds['train_path'] = [os.path.join(shape_dir, file) for file in ds['Train_Name']]


# Create log
log_path = 'execute_seedling_detection/runtime_log.txt'
with open(log_path, 'w+') as f:
    f.write(f'Project,Runtime,Size\n')


# Iterate over orthos
for ortho, size in zip(ds['ortho_path'], ds['size']):

    # Start runtime
    start = datetime.datetime.now()

    # Execute seedling detection for the project
    ortho_name = os.path.splitext(os.path.basename(ortho))[0]
    recognize_seedlings(ortho_path=ortho,
                        ml_model=ml_model,
                        output_dir=os.path.join(results_dir, ortho_name))

    # Log the runtime
    with open(log_path, 'a') as f:
        runtime_s = (datetime.datetime.now() - start).total_seconds()
        f.write(f'{ortho_name},{runtime_s},{size}\n')

