import os
import numpy as np
import fiona
import rasterio
from shapely.geometry import Point, asShape


def accuracy_assessment(ortho_path, det_points_path, ref_points_path, ref_plots_path):
    """
    This function checks how many ref_points have been detected within the given ref_plots.
    It also checks how many misclassifications have been done.
    :param ortho_path: path to the original orthophoto
    :param det_points_path: path to shapefile with the detected points
    :param ref_points_path: path to shapefile with the reference points
    :param ref_plots_path: path to shapefile with the reference plots
    :return:
    """

    # Import features
    ds = rasterio.open(ortho_path)

    with fiona.open(det_points_path, 'r') as source:
        det_points = [asShape(f['geometry']).simplify(1)
                      for f in list(source)]
        det_points = [p for p in det_points if p.geometryType() == 'Point']

    with fiona.open(ref_points_path, 'r') as source:
        ref_points = [asShape(f['geometry']).simplify(1)
                      for f in list(source)]
        ref_points = [p for p in ref_points if p.geometryType() == 'Point']

    with fiona.open(ref_plots_path, 'r') as source:
        ref_polygons = [asShape(f['geometry']).simplify(1)
                        for f in list(source) if f['geometry'] is not None]
        ref_polygons = [p for p in ref_polygons if p.geometryType() == 'Polygon']

    # Filter out points which are not inside the aoi
    det_points_aoi = []
    for p in det_points:
        if any([p.within(poly) for poly in ref_polygons]):
            det_points_aoi.append(p)

    # Transform to pixel coordinates
    det_points_px = [Point(ds.index(p.x, p.y)) for p in det_points_aoi]
    ref_points_px = [Point(ds.index(p.x, p.y)) for p in ref_points]

    n_detections = []

    for rp in ref_points_px:

        # Buffer the ref point
        rp_buffer = rp.buffer(40)

        # Get all detected points which are within the buffer
        neighbours = [p for p in det_points_px if p.within(rp_buffer)]

        # Append the number of detections
        n_detections.append(len(neighbours))

    # Get Statistics
    unique, counts = np.unique(n_detections, return_counts=True)
    classifications = dict(zip(unique, counts))
    n_correct_detections = classifications[1]
    keys_double = [k for k, v in classifications.items() if k >= 2]
    n_double_detections = np.sum([classifications[k] for k in keys_double])

    # Calculate statistics about the detected seedlings
    overall_accuracy = len(det_points_px) / len(ref_points_px)  # How accuracte is the guess of number of seedlings

    p_detected_ref_points = n_correct_detections / len(ref_points_px)  # How many of the seedlings have been detected
    p_dclass_ref_points = n_double_detections / len(ref_points_px)
    p_missed_ref_points = (len(ref_points_px) - n_correct_detections - n_double_detections * 2) / len(ref_points_px)

    # Calculate statistics about the results
    p_correct_points = n_correct_detections / len(det_points_px)  # How many of the seedlings have been detected
    p_double_points = n_double_detections / len(det_points_px)
    p_false_points = (len(det_points_px) - n_correct_detections - n_double_detections * 2) / len(det_points_px)

    return f'{overall_accuracy},' \
           f'{p_detected_ref_points},{p_dclass_ref_points},{p_missed_ref_points},' \
           f'{p_correct_points},{p_double_points},{p_false_points}'


if __name__ == '__main__':
    ortho_path = os.path.abspath(r'Z:\z_Helge\Ortho\F0060B-1.tif')
    det_points_path = os.path.abspath(r'Z:\z_Helge\Results\F0060B-1\F0060B-1_filtered_features.shp')
    ref_points_path = os.path.abspath(r'Z:\KEY ACCOUNTS\ABP_AUS\2003_ABP_SeedlingEastAustralia\F0060B\4_GIS\Referenzdaten\2003_ABP_Seedlings_F0060B_1_ReferenceTrees_AG_v1.shp')
    ref_plots_path = os.path.abspath(r'Z:\KEY ACCOUNTS\ABP_AUS\2003_ABP_SeedlingEastAustralia\F0060B\4_GIS\Referenzdaten\2003_ABP_Seedlings_F0060B_1_ReferencePolygon_AG_v1.shp')
