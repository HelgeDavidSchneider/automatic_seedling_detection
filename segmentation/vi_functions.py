import os
import numpy as np
from numpy import inf
import rasterio
from rasterio.windows import Window
from segmentation.image_operations import normalize_image
import cv2


def calculate_RGBVI(img):
    """
    Calculates the RGBVI Vegetation Index for an image.
    Formula:
    RGBVI = ((G * G) - (R * B)) / (G * G) + (R * B)

    Taken from:
    ttps://stackoverflow.com/questions/929103/convert-a-number-range-to-another-range-maintaining-ratio
    :param img: image as numpy array
    :return:
    """

    # Calculate the RGBVI by the formula
    img = img + 0.00001  # Add infenitesimal small number to avoid zero division error
    gg = img[:, :, 1] * img[:, :, 1]  # g * g
    rb = img[:, :, 0] * img[:, :, 2]  # r * b
    rgbvi = (gg - rb) / (gg + rb)

    return rgbvi


def calculate_GLI(img):
    """
    Calculates the VARI Vegetation Index for an image.
    Formula:
    GLI = (2 * G – R – B) / (2 * G + R + B)

    :param img: image as numpy array
    :return:
    """

    # Calculate the RGBVI by the formula
    img = img + 0.00001  # Add infenitesimal small number to avoid zero division error
    gli = (2 * img[:, :, 1] - img[:, :, 0] - img[:, :, 2]) / (2 * img[:, :, 1] + img[:, :, 0] + img[:, :, 2])

    return gli


def calculate_VARI(img):
    """
    Calculates the VARI Vegetation Index for an image.
    Formula:
    RGBVI = (G - R) / (G + R - B)

    :param img: image as numpy array
    :return:
    """

    # Calculate the RGBVI by the formula
    zaehler = img[:, :, 1] - img[:, :, 0]
    nenner = img[:, :, 1] + img[:, :, 0] - img[:, :, 2]
    vari = zaehler / nenner
    vari[nenner == 0] = 0  # Replace possible zero divisions with zero value

    # Replace nan with zero
    vari[np.isnan(vari)] = 0
    vari[vari == np.inf] = 0

    # Remove outliers
    vari[vari > 30] = 10

    return vari.astype(np.float16)


def calculate_NGRDI(img):
    """
    Calculates the VARI Vegetation Index for an image.
    Formula:
    NGRDI = (G - R) / (G + R)

    :param img: image as numpy array
    :return:
    """

    # Calculate the NGRDI by the formula
    zaehler = img[:, :, 1] - img[:, :, 0]
    nenner = img[:, :, 1] + img[:, :, 0]
    nenner[nenner == 0] = 0.001

    ngrdi = zaehler / nenner
    ngrdi[nenner == 0] = 0

    # Replace nan with zero
    ngrdi[np.isnan(ngrdi)] = 0
    ngrdi[ngrdi == np.inf] = 0

    ngrdi[ngrdi > 10] = 10

    return ngrdi.astype(np.float16)


def normalize_vi(img):

    # Normalize VI and convert to uint8
    img[img == np.inf] = 0
    img = normalize_image(img)
    img = img.astype(np.uint8)

    return img


def create_vegetation_masks(ortho, output, index_func):

    # Import Orthophoto
    ds = rasterio.open(ortho)

    # Get dynamic window size from the ortho with the shape around 1000 x 1000 pixel
    n_rit = int(ds.height / 1000)  # number of row iterations
    rstep = int(ds.height / n_rit)  # row window size
    n_cit = int(ds.width / 1000)  # number of row iterations
    cstep = int(ds.width / n_cit)  # column window size

    # Create Output Variables to store the intermediate results (vegetation mask and all detected features)
    veg_index_mem = np.memmap(os.path.join(os.path.dirname(output), f'veg_index_{index_func.__name__}_mem.dat)'),
                              dtype=np.float16, mode='w+', shape=(ds.height, ds.width))

    # Moving window over the ortho
    for r in np.arange(n_rit) * rstep:
        print(f'Row {r} from {n_rit * rstep}')  # Print Progress
        for c in np.arange(n_cit) * cstep:

            # Take subsample from the moving window
            img_sample = np.dstack([ds.read(1, window=Window(c, r, cstep, rstep)),
                                    ds.read(2, window=Window(c, r, cstep, rstep)),
                                    ds.read(3, window=Window(c, r, cstep, rstep))])

            # Calculate Veg Index
            img_sample_index = index_func(img_sample)

            # Save to image
            veg_index_mem[r:r + rstep, c:c + cstep] = img_sample_index

    # Normalize VI and convert to uint8
    veg_index_mem[veg_index_mem == inf] = 0
    veg_index_mem = cv2.GaussianBlur(veg_index_mem, (3, 3), 0)
    veg_index_mem = normalize_image(veg_index_mem)
    veg_index_mem = veg_index_mem.astype(np.uint8)

    # Save the Vegetation Index as Geotiff
    with rasterio.Env():

        # Take the profile from the original source and update it
        profile = ds.profile
        profile.update(dtype=rasterio.uint8, count=1)

        with rasterio.open(output, 'w', **profile) as dst:
            dst.write(veg_index_mem.astype(rasterio.uint8), 1)


if __name__ == '__main__':

    create_vegetation_masks(ortho=os.path.abspath('E:/Ortho/F0233B-3.tif'),
                            output=os.path.abspath('E:/Projects/F0233B-3_VARI.tif'),
                            index_func=calculate_VARI)
