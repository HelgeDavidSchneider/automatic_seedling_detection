"""
This script extracts small seedling, weed and ground images with a specific format from an orthophoto
for multiple projects given in a trainings data sheet.
All images are saved in the trainings_data folder and can be used for the training of a ml model.
"""

import os
import numpy as np
import pandas as pd
from trainings_data.create_trainigs_data import extract_img_samples, clear_img_repo

# Import Trainingsdata sheet
sheet_path = 'trainings_data/sheets/gtfp_pine.csv'
ds = pd.read_csv(sheet_path)
ds = ds.replace(np.nan, '', regex=True)  # Replace nan with empty string
ds = ds[ds['Train_Name'] != '']  # Only keep projects which have existing trainings data

# Append file type extenstions and join paths
ds['Ortho_Name'] = ds['Ortho_Name'] + '.tif'
ds['Train_Name'] = ds['Train_Name'] + '.shp'
ds['ortho_path'] = [os.path.join(dir, file) for dir, file in zip(ds['Ortho_Folder'], ds['Ortho_Name'])]
ds['train_path'] = [os.path.join(dir, file) for dir, file in zip(ds['Train_Folder'], ds['Train_Name'])]

# Iterate over projects and create the trainingsdata
trainings_folder = os.path.normpath('Z:/METHODIK/Machine_Learning_Models/Trainings_Data/GTFP_Pine_V2')
# clear_img_repo(trainings_folder)


# Create the trainingsdata for every project in the sheet
for name, ortho, train in zip(ds['Project Name'], ds['ortho_path'], ds['train_path']):

    print(f'Processing: {ortho}')

    if not os.path.exists(ortho):
        print(f'{ortho} does not  exist.\nSkipping project')
        continue

    if not os.path.exists(train):
        print(f'{train} does not  exist.\nSkipping project')
        continue

    extract_img_samples(ortho=ortho,
                        trainingsdata=train,
                        radius=7,
                        output_dir=trainings_folder,
                        seedling_type='pine',
                        augmentation=True,
                        n_max=5000)
