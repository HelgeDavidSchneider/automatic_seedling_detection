"""
This script executes the seedling recognition on an orthogoto with an already trained ml model.
"""

import os
from pathlib import Path
from centroid_detection.centroid_detection import detect_vegetation_peaks
from classifier_model.classify_objects import classify_objects_from_shapefile
from point_filter.point_filter import filter_points_from_shapefile


def recognize_seedlings(ortho_path='path/to/ortho.tif',
                        ml_model='path/to/model',
                        output_dir='path/to/folder'):
    """
    This function uses a VI and ml model to detect vegetation on an orthofoto.
    :param ortho_path: Path to the orthophoto
    :param ml_model: Path to the ml model as joblib
    :param output_dir: Output dir to save the results and intermediate results
    :return:
    """

    # Create the path variables to store the output daa
    ortho_name = os.path.splitext(os.path.basename(ortho_path))[0]
    if not Path(output_dir).is_dir():
        Path(output_dir).mkdir(parents=True, exist_ok=True)

    rgbvi = os.path.join(output_dir, f'{ortho_name}_rgbvi.tif')
    detected_features = os.path.join(output_dir, f'{ortho_name}_detected_features.shp')
    classified_features = os.path.join(output_dir, f'{ortho_name}_classified_features.shp')
    filtered_features = os.path.join(output_dir, f'{ortho_name}_filtered_features.shp')

    # Detect vegetation peaks from the orthophoto
    detect_vegetation_peaks(ortho_path=ortho_path,
                            peak_distance=10,
                            output_rgbvi_path=rgbvi,
                            output_objects_path=detected_features)

    # Classify the detected peaks (Radius has to match the classification model)
    classify_objects_from_shapefile(ortho_path=ortho_path,
                                    object_centroids=detected_features,
                                    radius=3,
                                    ml_model=ml_model,
                                    output_path=classified_features)

    # Filter the classified points
    filter_points_from_shapefile(centroids=classified_features,
                                 ortho_path=ortho_path,
                                 threshold=60,
                                 output_filtered_points=filtered_features)

    return
